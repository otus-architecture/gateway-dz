import { Test, TestingModule } from '@nestjs/testing';
import { HealthController } from './health.controller';
import { HealthService } from './health.service';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [HealthController],
      providers: [HealthService],
    }).compile();
  });

  describe('getHealth', () => {
    it(`should return { status: 'ok' }`, () => {
      const healthController = app.get(HealthController);
      expect(healthController.getHealth()).toStrictEqual({ status: 'OK' });
    });
  });
});
