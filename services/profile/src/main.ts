import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { Logger } from 'nestjs-pino';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.useLogger(app.get(Logger));
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  const configService = app.get(ConfigService);

  const host = configService.get('HOST');
  const port = configService.get('PORT');
  const mode = configService.get('NODE_ENV');
  const serviceHost = configService.get('SERVICE_HOST');
  const servicePort = configService.get('SERVICE_PORT');

  app.connectMicroservice<MicroserviceOptions>(
    {
      transport: Transport.TCP,
      options: {
        host: serviceHost,
        port: servicePort,
      },
    },
    { inheritAppConfig: true },
  );
  await app.startAllMicroservices();
  await app.listen(port, host);
  console.log(`Listen on ${host}:${port} in ${mode} mode`);
}

bootstrap();
