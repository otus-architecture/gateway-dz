import { Injectable } from '@nestjs/common';
import { collectDefaultMetrics, register } from 'prom-client';

@Injectable()
export class MetricsService {
  constructor() {
    collectDefaultMetrics();
  }

  public metrics() {
    return register.metrics();
  }
}
