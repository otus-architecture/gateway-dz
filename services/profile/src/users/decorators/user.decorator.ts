import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';

export const User = createParamDecorator((data: string, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest() as Request;
  const headers = request.headers;
  const user = {
    id: headers?.['x-token-sub'],
  };

  return data ? user[data] : user;
});
