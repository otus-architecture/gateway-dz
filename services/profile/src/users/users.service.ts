import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserDocument } from './schemas/user.schema';
import * as crypto from 'node:crypto';
import { promisify } from 'node:util';
import { UserDto } from './dto/user.dto';

const randomBytesAsync = promisify(crypto.randomBytes);
const pbkdf2Async = promisify(crypto.pbkdf2);

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(createUserDto: CreateUserDto): Promise<UserDto> {
    const { email, password, ...rest } = createUserDto;
    if (await this.userExists(createUserDto.email)) {
      throw new Error('Email already exists');
    }
    const hash = await this.getHash(password);
    const user = await new this.userModel({ ...rest, email, hash }).save();

    return this.convertToUserDto(user);
  }

  async findAll(): Promise<UserDto[]> {
    const users = await this.userModel.find().exec();
    return users.map((user) => this.convertToUserDto(user));
  }

  async findOne(id: string): Promise<UserDto> {
    const user = await this.userModel.findById(id).exec();
    if (!user) {
      throw new Error('User not found');
    }
    return this.convertToUserDto(user);
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<UserDto> {
    const { password, ...rest } = updateUserDto;
    if (password) {
      const hash = await this.getHash(password);
      Object.assign(rest, { hash });
    }
    const user = await this.userModel.findByIdAndUpdate(id, rest, { new: true }).exec();
    if (!user) {
      throw new Error('User not found');
    }
    return this.convertToUserDto(user);
  }

  async remove(id: string): Promise<UserDto> {
    const user = await this.userModel.findByIdAndDelete(id).exec();
    if (!user) {
      throw new Error('User not found');
    }
    return this.convertToUserDto(user);
  }

  async validateUser(email: string, password: string): Promise<UserDto | null> {
    const user = await this.findByemail(email);
    if (!user) {
      return null;
    }
    const isPasswordValid = await this.validatePassword(user.hash, password);

    return isPasswordValid ? this.convertToUserDto(user) : null;
  }

  private async userExists(email: string): Promise<boolean> {
    const user = await this.findByemail(email);
    return !!user;
  }

  private findByemail(email: string) {
    return this.userModel.findOne({ email });
  }

  private async getHash(password: string): Promise<string> {
    const salt = (await randomBytesAsync(16)).toString('hex');
    const hash = await pbkdf2Async(password, salt, 1000, 64, 'sha512');

    return `${salt}:${hash.toString('hex')}`;
  }

  private async validatePassword(dbHash: string, password: string): Promise<boolean> {
    const [salt, storedPassword] = dbHash.split(':');
    const hash = await pbkdf2Async(password, salt, 1000, 64, 'sha512');

    return crypto.timingSafeEqual(hash, Buffer.from(storedPassword, 'hex'));
  }

  private convertToUserDto(user: UserDocument): UserDto {
    return {
      id: user._id.toString(),
      email: user.email,
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName,
      phone: user.phone,
    };
  }
}
