import { Controller, Get, Post, Body, Param, Delete, Put, ForbiddenException } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './decorators/user.decorator';
import { UserDto } from './dto/user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get(':id')
  async findOne(@Param('id') id: string, @User() { id: userId }: UserDto) {
    if (id !== userId) {
      throw new ForbiddenException();
    }

    const user = await this.usersService.findOne(id);
    return user;
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto, @User() { id: userId }: UserDto) {
    if (id !== userId) {
      throw new ForbiddenException();
    }

    const user = await this.usersService.update(id, updateUserDto);
    return user;
  }

  @Delete(':id')
  async remove(@Param('id') id: string, @User() { id: userId }: UserDto) {
    if (id !== userId) {
      throw new ForbiddenException();
    }

    const user = await this.usersService.remove(id);
    return user;
  }
}
