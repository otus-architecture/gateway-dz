import { MiddlewareConsumer, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { HealthModule } from './health/health.module';
import { AuthModule } from './auth/auth.module';
import { JWKSModule } from './jwks/jwks.module';
import { LoggerModule } from 'nestjs-pino';
import { MetricsModule } from './metrics/metrics.module';
import { MonitorMiddleware } from './metrics/metrics.middleware';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    LoggerModule.forRoot(),
    HealthModule,
    AuthModule,
    JWKSModule,
    MetricsModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(MonitorMiddleware).forRoutes('*');
  }
}
