import { Body, Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { AuthService } from './auth/auth.service';
import { LocalAuthGuard } from './auth/guards/local-auth.guard';
import { JWKSService } from './jwks/jwks.service';

@Controller()
export class AppController {
  constructor(private readonly jwksService: JWKSService, private readonly authService: AuthService) {}

  @Get('jwks.json')
  jwks() {
    return this.jwksService.getStore().toJSON();
  }

  @Post('signup')
  async signup(@Body() createUserDto: any) {
    return this.authService.signup(createUserDto);
  }

  @UseGuards(LocalAuthGuard)
  @Post('signin')
  async login(@Request() req) {
    return this.authService.signin(req.user);
  }
}
