import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ClientProxy } from '@nestjs/microservices';
import { UserDto } from './dto/user.dto';

@Injectable()
export class AuthService {
  constructor(@Inject('PROFILE_SERVICE') private profileService: ClientProxy, private jwtService: JwtService) {}

  async signup(createUserDto: any) {
    return this.profileService.send<UserDto>({ cmd: 'create_user' }, createUserDto);
  }

  async signin(user: UserDto) {
    const payload = { email: user.email, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async validateUser(email: string, password: string) {
    return this.profileService.send<UserDto>({ cmd: 'validate_user' }, { email, password });
  }
}
