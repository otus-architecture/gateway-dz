import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { JWKSService } from 'src/jwks/jwks.service';
import { JWKSModule } from 'src/jwks/jwks.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: 'PROFILE_SERVICE',
        useFactory: (configService: ConfigService) => {
          return {
            transport: Transport.TCP,
            options: {
              host: configService.get('PROFILE_SERVICE_HOST'),
              port: configService.get('PROFILE_SERVICE_PORT'),
            },
          };
        },
        inject: [ConfigService],
      },
    ]),
    JwtModule.registerAsync({
      imports: [JWKSModule],
      useFactory: (store: JWKSService) => {
        const key = store.getStore().all()[0];

        return {
          signOptions: {
            algorithm: 'RS256',
            expiresIn: '1h',
            keyid: key.kid,
          },
          publicKey: key.toPEM(),
          privateKey: key.toPEM(true),
        };
      },
      inject: [JWKSService],
    }),
    PassportModule,
  ],
  providers: [AuthService, LocalStrategy],
  controllers: [],
  exports: [AuthService],
})
export class AuthModule {}
