import { Request, Response, NextFunction } from 'express';
import { Injectable, NestMiddleware } from '@nestjs/common';
import { Counter, Histogram } from 'prom-client';

@Injectable()
export class MonitorMiddleware implements NestMiddleware {
  private httpRequestDurationMicroseconds: Histogram;
  private httpResponseStatus: Counter;

  constructor() {
    this.httpRequestDurationMicroseconds = new Histogram({
      name: 'app_request_latency_ms',
      help: 'Duration of HTTP requests in ms',
      labelNames: ['method', 'endpoint', 'useragent', 'ip'],
      // buckets for response time from 0.1ms to 500ms
      buckets: [0.1, 5, 15, 50, 100, 200, 300, 400, 500],
    });
    this.httpResponseStatus = new Counter({
      name: 'app_request_count',
      help: 'Application Request Count',
      labelNames: ['method', 'endpoint', 'http_status'],
    });
  }

  use(request: Request, response: Response, next: NextFunction): void {
    const now = Date.now();
    const { ip, method, originalUrl: endpoint } = request;
    const userAgent = request.get('user-agent') || '';

    response.on('finish', () => {
      const responseTimeInMs = Date.now() - now;

      this.httpRequestDurationMicroseconds.labels(method, endpoint, userAgent, ip).observe(responseTimeInMs);
      this.httpResponseStatus.labels(method, endpoint, `${response.statusCode}`).inc();
    });

    next();
  }
}
