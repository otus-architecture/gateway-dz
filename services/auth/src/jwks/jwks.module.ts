import { Module } from '@nestjs/common';
import { JWKSService } from './jwks.service';
import { JWK } from 'node-jose';
import * as jwk from './jwk.json';

@Module({
  providers: [
    {
      provide: 'JWK_STORE',
      useFactory: async () => {
        const store = JWK.createKeyStore();
        const key = await JWK.asKey(jwk);
        await store.add(key);

        return store;
      },
    },
    JWKSService,
  ],
  exports: [JWKSService],
})
export class JWKSModule {}
