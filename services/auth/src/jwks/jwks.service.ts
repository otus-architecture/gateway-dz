import { Inject, Injectable } from '@nestjs/common';
import { JWK } from 'node-jose';

@Injectable()
export class JWKSService {
  constructor(@Inject('JWK_STORE') private readonly jwksKeystore: JWK.KeyStore) {}

  getStore() {
    return this.jwksKeystore;
  }
}
