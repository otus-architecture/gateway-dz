import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { Logger } from 'nestjs-pino/Logger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.useLogger(app.get(Logger));
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  const configService = app.get(ConfigService);

  const host = configService.get('HOST');
  const port = configService.get('PORT');
  const mode = configService.get('NODE_ENV');

  await app.listen(port, host);
  console.log(`Listen on ${host}:${port} in ${mode} mode`);
}

bootstrap();
