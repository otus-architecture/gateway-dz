# Auth service

## Local development
- clone the repository
- install all dependencies `npm i`
- create `.env` file and fill it as in `.env.example` file
- run the app `npm run start:dev`

## Local build and start
- run `npm run build`
- run `npm run start:prod`

## Docker local build
For building a docker image run `docker build -t YOUR_TAG .`

## Docker local run
For local build run the following command `docker run -p 127.0.0.1:YOUR_PORT:8000 YOUR_TAG`

You can pass your env file by using `docker run -p 127.0.0.1:YOUR_PORT:8000 --env-file .env YOUR_TAG`

## Run app using image from docker hub
Also you can run the app using `docker run -p 127.0.0.1:YOUR_PORT:8000 zahar80/dz2-docker`
