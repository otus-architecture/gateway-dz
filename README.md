# DZ GATEWAY

## Prerequisites
Install ambassador:

0. Add ambassador repo `helm repo add datawire https://app.getambassador.io && helm repo update`
1. Run `kubectl apply -f https://app.getambassador.io/yaml/edge-stack/3.5.1/aes-crds.yaml`
2. Wait for installing `kubectl wait --timeout=90s --for=condition=available deployment emissary-apiext -n emissary-system`
3. Run `helm install -n ambassador --create-namespace edge-stack datawire/edge-stack`
4. Wait for installing `kubectl rollout status -n ambassador deployment/edge-stack -w`

Install prometheus:

0. Add prometheus repo `helm repo add prometheus-community https://prometheus-community.github.io/helm-charts && helm repo update`
1. Install prometheus `helm install -n monitoring --create-namespace prom prometheus-community/kube-prometheus-stack -f deploy/prometheus/prometheus.yaml --atomic`

## To create the project
- start profile microservice `helm install profile deploy/profile`
- start auth microservice `helm install auth deploy/auth`
- add ambassador rules and metrics `kubectl apply -f deploy/ambassador`
- add app grafana dashboard `kubectl apply -f deploy/prometheus/grafana.yaml`

## To test the project
- run `newman run newman/test-auth.json`

## To delete the project
- delete auth microservice `helm uninstall auth`
- delete profile microservice `helm uninstall profile`
- delete ambassador chart `helm -n ambassador uninstall edge-stack`
- delete ambassador CRDs `kubectl delete -f https://app.getambassador.io/yaml/edge-stack/3.5.1/aes-crds.yaml`
- delete prometheus `helm -n monitoring uninstall prom`

## Diagrams

[User signup diagram](assets/auth-signup.puml).

[User signin diagram](assets/auth-signin.puml).

[Interaction with user profile diagram](assets/profile-service.puml).
